import { wait } from "@testing-library/user-event/dist/utils";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { GoogleMap } from "react-google-maps";
let db = "https://623dc231db0fc039d4bbfbad.mockapi.io/";
let favs = getFav();

export const AuthContext = React.createContext({
  userLogged: true,
  setUserLogged: () => undefined,
});

export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState(false);

  const value = {
    currentUser,
    setCurrentUser,
    signup,
    login,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export async function login(username, password) {
  let snapshot = await axios.get(db + `login`);
  let obj = snapshot.data;
  let account = obj.find((element) => element.userName == username);
  console.log(`${account.userName}`);
  if (account.userName === username && account.password == password) {
    // return console.log(account);
    return true;
  } else {
    return false;
  }
}

export function signup(userName, Password) {
  axios
    .post(db + `login`, {
      userName: `${userName}`,
      password: `${Password}`,
    })
    .then((response) => {
      return console.log(response);
    });
}

export async function favPlaces(place, favs) {
  let spot = place.id;
  let exists = false;
  console.log(favs);
  favs.forEach((element) => {
    if (element.spot === spot) {
      console.log("true " + element.spot + "=" + spot);
      exists = true;
    } else {
      console.log("false " + element.spot + " is diff " + spot);
      exists = false;
    }
  });

  return exists;
}

export async function favLoc(selectedKite) {
  let favs = await axios.get(db + `favourites`);
  let loc = favs.data;

  console.log(Object.entries(loc));

  let spot = selectedKite.id;
  console.log(spot);
  let exist = false;
  for (let i = 0; i < Object.values(loc).length; i++) {
    if (Object.values(loc)[i].spot === spot) {
      console.log("true " + Object.values(loc)[i].spot + "=" + spot);
      exist = true;
    } else {
      console.log("false " + Object.values(loc)[i].spot + " is diff " + spot);
    }
  }
  // let snapshot = Object.entries(loc).find((element) => element[1].spot == spot);
  // console.log(snapshot);
  if (exist) {
    console.log(" it allready exists");
    return false;
  } else {
    axios
      .post(db + `favourites`, {
        spot: selectedKite.id,
      })
      .then((response) => {
        console.log(response);
      });
    return true;
  }
}

export async function remFav(kiteD) {
  await axios.delete(db + `favourites/${kiteD}`).then((response) => {
    console.log(response);
  });
}

export async function getFav() {
  await axios.get(db + `favourites`).then((response) => {
    return response;
  });
}

export async function spots() {
  await axios.get(db + `spot`).then((response) => {
    return response;
  });
}
