import React from "react";
import Dashboard from "./dashboard";
import Login from "./login";
import SignUp from "./signup";
import { ReactDOM } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
  Outlet,
} from "react-router-dom";
import "../styles/login.css";
import user from "../components/login";

export default function App() {
  return (
    <div className="root">
      <Router>
        <Routes>
          <Route path="/" element={<Login />} />
          {console.log(user)}
          <Route path="/signup" element={<SignUp />} />
          <Route path="/dashboard" element={<Dashboard />} />
        </Routes>
      </Router>
    </div>
  );
}
