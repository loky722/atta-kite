import React from "react";
import { useRef, useState, useContext } from "react";
import { ReactDOM } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
  Outlet,
} from "react-router-dom";
import "../styles/dashboard.css";
import { AuthContext } from "../functions";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { login } from "../functions";
let db = "https://623dc231db0fc039d4bbfbad.mockapi.io/";

export default function Login() {
  let history = useNavigate();
  const usernameRef = useRef();
  const passwordRef = useRef();
  const [error, setError] = useState("");
  let [stock, setNewStock] = useState();
  let [maps, setNewMaps] = useState();
  let [user, setUser] = useState(false);

  async function handleSubmit(e) {
    e.preventDefault();

    if (usernameRef.current.value == " ") {
      console.log("username missing");
    } else {
      try {
        setUser(login(usernameRef.current.value, passwordRef.current.value));
      } catch (err) {
        console.log(err);
      }
      if (user) {
        return history("/dashboard");
      } else {
        setError(
          <Link style={{ color: "red", opacity: 0.8 }} to="./signup">
            Username/Password incorect! Don't have a account, press here!
          </Link>
        );
        return history("/");
      }
    }
  }
  async function mapp(stock) {
    console.log(stock);
    let obj = Object.entries(stock);
    console.log(obj);
    let find = obj.filter((element) => parseInt(element[0]) < 8);
    let count = find.map((element) => (
      <tr>
        <td>{element[1].name}</td>
        <td>{element[1].country}</td>
        <td>{element[1].lat}</td>
        <td>{element[1].long}</td>
        <td>{element[1].probability}</td>
        <td>{element[1].month}</td>
      </tr>
    ));
    setNewMaps(count);
  }
  async function spots() {
    await axios.get(db + `spot`).then((response) => {
      setNewStock(response.data);
      mapp(response.data);
    });
  }

  return (
    <form onSubmit={handleSubmit} className="b">
      <h1 className="kite" root>
        Kite
      </h1>
      <h2 className="err">{error}</h2>
      <div className="i">
        <label className="input-label">Username</label>
        <input
          type="text"
          ref={usernameRef}
          className="input"
          id="username"
          required
        />
      </div>

      <div className="i">
        <label className="input-label">Password</label>
        <input
          type="password"
          ref={passwordRef}
          id="password"
          className="input"
        />
      </div>
      <div className="i">
        <button className="loginButton" type="submit">
          <p>Login</p>
        </button>
      </div>
    </form>
  );
}

// https://623dc231db0fc039d4bbfbad.mockapi.io/
