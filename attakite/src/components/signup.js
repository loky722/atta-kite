import React from "react";
import { useRef, useState, useContext } from "react";
import { ReactDOM } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
  Outlet,
} from "react-router-dom";
import "../styles/login.css";
import { AuthContext } from "../functions";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

let db = "https://623dc231db0fc039d4bbfbad.mockapi.io/";

export default function SignUp() {
  let history = useNavigate();
  const usernameRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const [error, setError] = useState("");
  let [stock, setNewStock] = useState();
  let [maps, setNewMaps] = useState();
  const { signup } = useContext(AuthContext);

  async function handleSubmit(e) {
    e.preventDefault();

    if (passwordRef.current.value == "") {
      setError("You must fill all forms!");
    } else if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      setError("Passwords do not match!");
    } else if (usernameRef.current.value === ``) {
      setError("You must fill all forms!");
    } else {
      try {
        await signup(usernameRef.current.value, passwordRef.current.value).then(
          (response) => {
            console.log(response);
          }
        );
        console.log(usernameRef.current);
      } catch (err) {
        console.log(err);
      }
      history("/dashboard");
    }
  }
  async function mapp(stock) {
    console.log(stock);
    let obj = Object.entries(stock);
    console.log(obj);
    let find = obj.filter((element) => parseInt(element[0]) < 8);
    let count = find.map((element) => (
      <tr>
        <td>{element[1].name}</td>
        <td>{element[1].country}</td>
        <td>{element[1].lat}</td>
        <td>{element[1].long}</td>
        <td>{element[1].probability}</td>
        <td>{element[1].month}</td>
      </tr>
    ));
    setNewMaps(count);
  }
  async function spots() {
    await axios.get(db + `spot`).then((response) => {
      setNewStock(response.data);
      mapp(response.data);
    });
  }

  return (
    <form onSubmit={handleSubmit} className="b">
      <h1 className="kite" root>
        Kite
      </h1>
      <h2 className="err">{error}</h2>
      <div className="i">
        <label className="input-label">Username</label>
        <input type="text" ref={usernameRef} className="input" id="username" />
      </div>

      <div className="i">
        <label className="input-label">Password</label>
        <input
          type="password"
          ref={passwordRef}
          id="password"
          className="input"
        />
      </div>
      <div className="i">
        <label className="input-label">Confirm password</label>
        <input
          type="password"
          ref={passwordConfirmRef}
          id="password"
          className="input"
        />
      </div>
      <div className="i">
        <button className="loginButton" type="submit">
          <p>SignUp</p>
        </button>
      </div>
    </form>
  );
}

// https://623dc231db0fc039d4bbfbad.mockapi.io/
