import React, { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  useNavigate,
  Outlet,
} from "react-router-dom";
import "../styles/dashboard.css";
import { FaKickstarter, FaUserCircle } from "react-icons/fa";
import { CgSortAz } from "react-icons/cg";
import { RiLoginBoxLine } from "react-icons/ri";
import {
  GoogleMap,
  useLoadScript,
  Marker,
  InfoWindow,
  DataProps,
} from "@react-google-maps/api";
import { useRef } from "react";

import { AiOutlineSearch } from "react-icons/ai";
import axios from "axios";
import { favLoc, favPlaces, remFav } from "../functions";
import favMap from "../styles/FavMap.png";
let db = "https://623dc231db0fc039d4bbfbad.mockapi.io/";

export default function Dashboard() {
  let [selectedKite, setSelectedKite] = useState(null);
  let [stock, setNewStock] = useState("");
  let [maps, setNewMaps] = useState("");
  let [displayAddSpot, setDisplayAddSpot] = useState(false);
  let [open, setOpen] = useState(false);
  let [spot, setSpot] = useState(false);
  let [userM, setUserM] = useState(false);
  let [favs, setFavs] = useState([]);
  let count = 0;
  console.log(favs);

  let navFilter;
  let addSpotMenu;
  let userMenu;

  const search = useRef();
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: "AIzaSyBsDebtfDoEGNcLgNW-WlTetejigM74QC4",
    libraries: ["places"],
  });

  useEffect(() => {
    spots();
    getFav();
  }, []);

  if (userM) {
    userMenu = (
      <div className="userButton">
        <button className="userButton" onClick={() => setUserM(!userM)}>
          <RiLoginBoxLine size={37} />
          Logout
        </button>
      </div>
    );
  }

  if (spot) {
    addSpotMenu = (
      <div className="addSpotMenu">
        <a className="filterInput">Add Spot</a>
        <label>Name</label>
        <input className="filterInput"></input>
        <label className="mt-15px">Country</label>
        <input className="filterInput"></input>
        <label className="mt-15px">Hight Season</label>
        <input className="filterInput" type="date"></input>
        <GoogleMap
          zoom={8}
          center={{ lat: 44, lng: 28 }}
          mapContainerClassName="map-container-addSpot"
        ></GoogleMap>
        <ul className="CCbuttons">
          <button className="addSpotButtonC" onClick={() => setSpot(!spot)}>
            CANCEL
          </button>{" "}
          <button className="addSpotButtonCO">CONFIRM</button>
        </ul>
      </div>
    );
  } else {
    addSpotMenu = "";
  }

  if (open) {
    navFilter = (
      <div className="dbMenu">
        <label>Country</label>
        <input className="filterInput"></input>
        <label>Wind Probability</label>
        <input className="filterInput"></input>
        <button className="applyFilter" onClick={() => setOpen(!open)}>
          APPLY FILTER
        </button>
      </div>
    );
  } else {
    navFilter = (
      <button className="filter " onClick={() => setOpen(open)}>
        <CgSortAz size={29} />
        FILTERS
      </button>
    );
  }

  async function getFav() {
    await axios.get(db + `favourites`).then((response) => {
      setFavs(response.data);
    });
  }

  async function spots() {
    await axios.get(db + `spot`).then((response) => {
      mapp(response.data);
      setNewStock(response.data);
    });
  }

  function wait(ms) {
    var start = new Date().getTime();
    var end = start;
    while (end < start + ms) {
      end = new Date().getTime();
    }
  }

  function kiteInfo() {
    let k = favs.find((place) => place.spot === selectedKite.id);
    if (k) {
      return (
        <InfoWindow
          position={{
            lat: parseInt(selectedKite.lat),
            lng: parseInt(selectedKite.long),
          }}
          onCloseClick={() => {
            setSelectedKite(null);
          }}
        >
          <div>
            <h1>{selectedKite.name}</h1>
            <p className="country">{selectedKite.country}</p>
            <p className="title"> WIND PROBABILITY</p>
            <p className="info ">{selectedKite.probability}</p>
            <p className="title">LATITUDE</p>
            <p className="info ">{selectedKite.lat}</p>
            <p className="title">LONGITUDE</p>
            <p className="info ">{selectedKite.long}</p>
            <p className="title">WHEN TO GO</p>
            <p className="info ">{selectedKite.month}</p>
            <button
              className="markerMap"
              onClick={() => {
                favLoc(selectedKite);
                // favs.push({ spot: selectedKite.id });
                let iD;
                for (let i = 0; i < favs.length; i++) {
                  if (favs[i].spot === selectedKite.id) {
                    iD = favs[i].id;
                  }
                }
                let place = favs.filter(function (el) {
                  return el.spot != selectedKite.id;
                });
                console.log(iD);
                remFav(iD);
                // favs.slice(favs[iD], 1);
                console.log(iD, selectedKite.id);
                setFavs(place);
                setSelectedKite(null);
              }}
            >
              - REMOVE FROM FAVOURITES
            </button>
          </div>
        </InfoWindow>
      );
    } else {
      return (
        <InfoWindow
          position={{
            lat: parseInt(selectedKite.lat),
            lng: parseInt(selectedKite.long),
          }}
          onCloseClick={() => {
            setSelectedKite(null);
          }}
        >
          <div>
            <h1>{selectedKite.name}</h1>
            <p className="country">{selectedKite.country}</p>
            <p className="title"> WIND PROBABILITY</p>
            <p className="info ">{selectedKite.probability}</p>
            <p className="title">LATITUDE</p>
            <p className="info ">{selectedKite.lat}</p>
            <p className="title">LONGITUDE</p>
            <p className="info ">{selectedKite.long}</p>
            <p className="title">WHEN TO GO</p>
            <p className="info ">{selectedKite.month}</p>
            <button
              className="favoritesMap"
              onClick={() => {
                favLoc(selectedKite);
                favs.push({ spot: selectedKite.id });
                setSelectedKite(null);
              }}
            >
              + ADD TO FAVORITES
            </button>
          </div>
        </InfoWindow>
      );
    }
  }

  async function mapp(stock) {
    console.log(stock);
    let obj = Object.entries(stock);
    console.log(obj);
    let find = obj.filter((element) => parseInt(element[0]) < 8);
    let count = find.map((element) => (
      <tr>
        <td>{element[1].name}</td>
        <td>{element[1].country}</td>
        <td>{element[1].lat}</td>
        <td>{element[1].long}</td>
        <td>{element[1].probability}</td>
        <td>{element[1].month}</td>
      </tr>
    ));
    setNewMaps(count);
  }

  // async function sortPlaces(stock) {
  //   let obj = Object.entries(stock);
  //   let find = obj.filter((element) => parseInt(element[0]) < 8);
  //   console.log(find);
  //   mapp(find);
  // }
  // sortPlaces(stock);

  if (loadError) return "Error loading map";
  if (!isLoaded) return "Loading Maps";
  if (!stock) return "Loading Stocks";
  if (!maps) return "Loading";

  return (
    <>
      <header>
        <a className="kiteD">Kite</a>
        <nav>
          <ul className="nav_links">
            <li>
              {" "}
              <a>
                <button className="addSpot" onClick={() => setSpot(!spot)}>
                  ADD SPOT
                </button>
              </a>{" "}
            </li>
            <li>
              <a>
                <button
                  className="user"
                  onClick={() => {
                    setUserM(!userM);
                  }}
                >
                  <FaUserCircle size={51} />
                </button>
              </a>
            </li>
          </ul>
          <div className="logout">{userM && userMenu}</div>
        </nav>
      </header>
      <div
        style={{
          width: "100vw",
          height: "100vh",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <GoogleMap
          zoom={3}
          center={{ lat: 44, lng: 28 }}
          mapContainerClassName="map-container"
        >
          {stock.map((element) => {
            let k = favs.find((place) => place.spot === element.id);

            // console.log( k, count, element.id);
            if (k) {
              count++;
            }

            if (k) {
              return (
                <Marker
                  key={k.spot}
                  position={{
                    lat: parseInt(element.lat),
                    lng: parseInt(element.long),
                  }}
                  onClick={() => {
                    setSelectedKite(element);
                  }}
                  icon={{
                    url: favMap,
                    scaledSize: new window.google.maps.Size(25, 40),
                  }}
                />
              );
            } else {
              return (
                <Marker
                  key={element.id}
                  position={{
                    lat: parseInt(element.lat),
                    lng: parseInt(element.long),
                  }}
                  onClick={() => {
                    setSelectedKite(element);
                  }}
                />
              );
            }
          })}
          {/* {stock.map((element) =>
            favPlaces(element, favs) ? (
              <Marker
                key={element.id}
                position={{
                  lat: parseInt(element.lat),
                  lng: parseInt(element.long),
                }}
                onClick={() => {
                  setSelectedKite(element);
                }}
                icon={{
                  url: favMap,
                  scaledSize: new window.google.maps.Size(25, 40),
                }}
              />
            ) : (
              <Marker
                key={element.id}
                position={{
                  lat: parseInt(element.lat),
                  lng: parseInt(element.long),
                }}
                onClick={() => {
                  setSelectedKite(element);
                }}
              />
            )
          )} */}

          {selectedKite && kiteInfo()}
          {open && navFilter}
          {!open && (
            <button className="filter " onClick={() => setOpen(!open)}>
              <CgSortAz size={29} />
              FILTERS
            </button>
          )}
          {spot && addSpotMenu}
        </GoogleMap>
        <h1 className="location">Locations</h1>
        <div className="d-flex">
          <label className="search">
            <i aria-hidden="true">
              {" "}
              <AiOutlineSearch />
            </i>
          </label>
          <input
            type="text"
            ref={search}
            className="searchb"
            placeholder="Search..."
            id="search"
          />
        </div>
        <div className="scrollmenu">
          <table>
            <tr>
              <th className="top">
                Name
                <select></select>
              </th>
              <th className="top">
                Country
                <select></select>
              </th>

              <th className="top">
                Latitude
                <select></select>
              </th>

              <th className="top">
                Longitude
                <select></select>
              </th>

              <th className="top">
                Wind Prob.
                <select></select>
              </th>

              <th className="top">
                When to go
                <select></select>
              </th>
            </tr>
            {maps}
          </table>
        </div>
      </div>
    </>
  );
}
